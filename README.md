# Angular course project

Курсов проект по Абстракции и преизползваем код в клиентска среда


### Кратко описание на проекта

Проекта представлява single page application за търсене на информация за филми, запазването им в "Любими" и записване на изгледани филми.


Използва се [omdbapi](https://omdbapi.com) за получаване на информация за филмите.

### Инсталация
```bash
# Clone repository
$ git clone https://gitlab.com/peterlyubenov/angular-project
# cd into the project dir
$ cd angular-project
# Install dependencies
$ npm install
# Run fake backend
$ npx json-server src/db.json
# Run angular
$ ng serve

```


### Данни за студента

Петър Любенов
ф. №: 1709010883 
Софтуерно Инженерство, III курс
