import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IndexComponent } from './pages/index/index.component';
import { SearchComponent } from './pages/search/search.component';
import { SavedComponent } from './pages/saved/saved.component';
import { DetailsComponent } from './pages/details/details.component';
import { SeenComponent } from './pages/seen/seen.component';
import { AboutComponent } from './pages/about/about.component';


const routes: Routes = [
  { path: '', redirectTo: "/home", pathMatch: "full" },
  { path: 'home', component: IndexComponent },
  { path: 'search', component: SearchComponent },
  { path: 'saved', component: SavedComponent },
  { path: 'seen', component: SeenComponent },
  { path: 'details/:id', component: DetailsComponent },
  { path: 'about', component: AboutComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
