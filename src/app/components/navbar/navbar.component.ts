import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  constructor(private router: Router, private route: ActivatedRoute) { }
  
  public query = '';
  public currentRoute = "home";

  ngOnInit(): void {
    this.route.queryParams.subscribe(params => {
      if(params['q']) {
        this.query = decodeURI(params['q']);
      }
    })

    this.router.events.subscribe(event => {
      if(event instanceof NavigationEnd) {
        this.currentRoute = event.url;
      }
    })
  }

  public searchSubmit(event) {
    event.preventDefault();

    const movieName = (document.querySelector('input[name="movieSearchField"]') as HTMLInputElement).value;
    // this.searchData.searchQuery = movieName;
    this.router.navigate(['search'], { queryParams: { q: encodeURI(movieName) }});
    

  }

}
