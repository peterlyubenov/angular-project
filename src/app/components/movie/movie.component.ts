import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import Movie from '../../models/Movie';
import { MovieService } from 'src/app/services/movie.service';

@Component({
  selector: 'app-movie',
  templateUrl: './movie.component.html',
  styleUrls: ['./movie.component.scss']
})
export class MovieComponent implements OnInit {

  constructor(private movieService: MovieService) { }

  @Input() movie: Movie;
  @Input() unsaveButton = false;
  @Input() unseeButton = false;
  @Output() unsaved = new EventEmitter<number>();
  @Output() unseen = new EventEmitter<number>();
  public isSaved: boolean;
  public isSeen: boolean;

  ngOnInit(): void {
    this.isSaved = this.checkIfSaved();
    this.isSeen = this.checkIfSeen();
  }

  save() {
    this.movieService.saveMovie(this.movie).subscribe(data => {
      this.isSaved = true;
    });
  }

  unsave() {
    if(this.unsaveButton) 
      this.unsaved.emit(this.movie.id);
  }

  seen() {
    if(this.isSeen) return;
    this.movieService.seeMovie(this.movie).subscribe(() => {
      this.isSeen = true;
    });
  }

  toggleSave() {
    if(this.isSaved) {
      this.unsave();
    } else {
      this.save();
    }
  }

  checkIfSaved(): boolean {
    return JSON.parse(window.localStorage.getItem('savedMovies')).includes(this.movie.imdbID);
  }

  checkIfSeen(): boolean {
    return JSON.parse(window.localStorage.getItem('seenMovies')).includes(this.movie.imdbID);
  }

}
