import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import Movie from '../../models/Movie';
import { OmdbService } from '../../services/omdb.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  constructor(private route: ActivatedRoute, private omdb: OmdbService) { }

  public results: Movie[];
  public error: string | null = null;

  ngOnInit(): void {
    this.route.queryParams.subscribe(routes => {
      
      const query = decodeURI(routes['q'])
      if(!routes['q']) {
        this.error = "no query";
        return;
      }

      this.omdb.search(query).subscribe(response => {
        if (response.Response === "True") {
          this.results = response.Search;
        } else {
          this.error = "not found";
        }
      });
    })
  }
}
