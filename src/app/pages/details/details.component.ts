import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import Movie from 'src/app/models/Movie';
import { MovieService } from 'src/app/services/movie.service';
import { OmdbService } from 'src/app/services/omdb.service';
import OmdbResult from 'src/app/models/OmdbGetResult';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {

  constructor(private route: ActivatedRoute, private movieService: MovieService, private omdb: OmdbService) { }

  public movie: OmdbResult;
  public found = true;
  public isFavorite: { favorite: boolean, id?: number };
  public isSeen: { seen: boolean, id?: number };

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.omdb.getById(params.id).subscribe((movie: OmdbResult) => {
        this.movie = movie;

        if(movie.Response === 'False') {
          this.found = false;
          return;
        }

        // Check if the movie is favorited by attempting to fetch it from the saved movies database
        this.movieService.getSavedMovie(params.id).subscribe((data: Movie[]) => {
          if(data.length > 0) {
            this.isFavorite = { favorite: true, id: data[0].id };
          } else {
            this.isFavorite = { favorite: false }
          }
        })

        // Check if the movie is seen
        this.movieService.getSeenMovie(params.id).subscribe((data: Movie[]) => {
          if(data.length > 0) {
            let movie = data.find(x => x.imdbID === params.id);
            if(movie) {
              this.isSeen = { seen: true, id: movie.id };
              return;
            }
          }
          this.isSeen = { seen: false };
        })
      })
    })
  }

  save() {
    this.movieService.saveMovie({
      Poster: this.movie.Poster,
      Title: this.movie.Title,
      Type: this.movie.Type,
      Year: this.movie.Year,
      imdbID: this.movie.imdbID
    }).subscribe(data => {
      this.isFavorite = { favorite: true, id: data.id }
    })
  }

  unsave() {
    this.movieService.unsaveMovie(this.isFavorite.id).subscribe(() => {
      this.isFavorite = { favorite: false }
    })
  }

  see() {
    this.movieService.seeMovie({
      Poster: this.movie.Poster,
      Title: this.movie.Title,
      Type: this.movie.Type,
      Year: this.movie.Year,
      imdbID: this.movie.imdbID
    }).subscribe((data) => {
      this.isSeen = { seen: true, id: data.id }
    })
  }

  unsee() {
    this.movieService.unseeMovie(this.isSeen.id).subscribe(() => {
      this.isSeen = { seen: false };
    })
  }

}
