import { Component, OnInit } from '@angular/core';
import { MovieService } from 'src/app/services/movie.service';
import Movie from 'src/app/models/Movie';

@Component({
  selector: 'app-seen',
  templateUrl: './seen.component.html',
  styleUrls: ['./seen.component.scss']
})
export class SeenComponent implements OnInit {

  constructor(private movieService: MovieService) { }

  public movies: Movie[];

  ngOnInit(): void {
    this.movieService.getSeenMovies().subscribe((movies: Movie[]) => {
      this.movies = movies;
    });
  }

  remove(id): void {
    this.movieService.unseeMovie(id).subscribe(() => {
      this.movies = this.movies.filter(x => x.id !== id);
    });
  }
}
