export default interface OmdbResult {
    Response: string,
    Error: string | null,
    Title?: string,
    Year?: string,
    Rated?: string,
    Runtime?:  string,
    Genre?: string,
    Director?: string,
    Writer?: string,
    Plot?: string,
    imdbID: string,
    imdbRating: string,
    Type: string,
    [movieDetails: string]: any
}