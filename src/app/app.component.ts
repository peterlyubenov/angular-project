import { Component } from '@angular/core';
import { MovieService } from './services/movie.service';
import { OnInit } from '@angular/core';
import Movie from './models/Movie';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'whatsup';

  constructor(private movieService: MovieService) {}

  ngOnInit() {
    this.movieService.updateSavedMovies();
  }
}
