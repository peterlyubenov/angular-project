import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { key } from './apikey';
import Movie from '../models/Movie';
import OmdbResult from '../models/OmdbGetResult';


@Injectable({
  providedIn: 'root'
})
export class OmdbService {
  
  private apiRoute = 'http://www.omdbapi.com/'

  constructor(private http: HttpClient) { }

  /**
   * Search the OMDb for movies that match a given string
   * @param searchString Movie title to search for
   */
  public search(searchString: string): Observable<any> {
    return this.http.get(this.apiRoute, { 
      params: {
        apikey: key,
        s: searchString,
        type: "movie"
      }
    });
  }

  /**
   * Gets information about a movie by its IMDB ID
   * @param imdbId The IMDB id of the movie to get
   */
  getById(imdbId: string): Observable<OmdbResult> {
    return this.http.get<OmdbResult>(this.apiRoute, {
      params: {
        apikey: key, 
        type: 'movie', 
        i: imdbId
      }
    })
  }
}
